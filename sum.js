var sum = 0;

var args = process.argv.slice(2);
//console.log(args);

args.forEach( function(val, index, array) {
  sum += Number(val);
  //console.log(val);
});

console.log(sum);


/* Recommended Solution
    var result = 0
    for (var i = 2; i < process.argv.length; i++)
      result += Number(process.argv[i])
    console.log(result)
*/
